<?php

require_once('../model/model.php');
require_once('../includes/helper.php');

if (isset($_SESSION['userid']) && isset($_POST['symbol']))
{
	$userid = $_SESSION['userid'];
	$_SESSION['symbol'] = $_POST['symbol'];
	$symbol = $_SESSION['symbol'];
	$_SESSION['shares'] = $_POST['shares'];
	$shares = $_SESSION['shares'];
	$sell = sell_shares($userid, $symbol, $shares);
	render('home');
}
else
	render('login');

?>
