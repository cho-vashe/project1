<?
/* Registration controller */

require_once('../model/model.php');
require_once('../includes/helper.php');

if (isset($_POST['email']) &&
	isset($_POST['password']))
	{
		$email = mysql_escape_string(strtolower($_POST['email']));
		$password = $_POST['password'];
		$_SESSION['userid'] = register_user($email, $password);
		$_SESSION['balance'] = get_user_balance($_SESSION['userid']);
		render('home');
		
	}
else
	{
		render('register');
	}

?>


