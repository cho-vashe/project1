<?php

require_once('../model/model.php');
require_once('../includes/helper.php');

//redirect to login if not logged in
if (!isset($_SESSION['userid']))
	render('login');
			
if (isset($_POST["symbol"]) && isset($_POST["shares"]))
	{
		$_SESSION['symbol'] = $_POST['symbol'];
		$symbol = $_SESSION['symbol'];
		$shares = $_POST['shares'];
		$userid = $_SESSION['userid'];
		$status = buy_shares($userid, $symbol, $shares);
		if ($status == 1)
		{
			render('home');
			echo ('<script type="text/javascript">alert ("Transaction was successful!");</script>');
		}
		else if ($status == 0)
		{
			render('quote', array('quote_data' => get_quote_data(urlencode($symbol), urlencode('sl1n'))));
			echo ('<script type="text/javascript">alert ("Not enough funds");</script>');
		}
		else if ($status == -1)
		{
			render('quote', array('quote_data' => get_quote_data(urlencode($symbol), urlencode('sl1n'))));
			echo ('<script type="text/javascript">alert ("Provide a positive integer");</script>');
		}	
	}

		
