<?php

require_once('../model/model.php');
require_once('../includes/helper.php');

if (isset($_SESSION['userid']))
	{
		$userid = $_SESSION['userid'];
		$sym_str = urlencode(get_user_symbols($userid)); //or space instead of plus?
		$data = get_quote_data($sym_str, urlencode('l1'));
		render('count', array('data' => $data));
		
	}
else
	render('login');
?>
