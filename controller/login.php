<?php
/*******************
 * login.php
 *
 * CSCI S-75
 * Project 1
 * Chris Gerber
 *
 * Login controller
 *******************/

require_once('../model/model.php');
require_once('../includes/helper.php');

if (isset($_POST['email']) &&
	isset($_POST['password']))
{
	$email = mysql_escape_string($_POST['email']);
	$password = $_POST['password'];
	$pwdhash = hash("SHA1", $password);
	$userid = login_user($email, $pwdhash);
	if ($userid > 0)
	{
		$_SESSION['userid'] = $userid;
		$_SESSION['balance'] = get_user_balance($_SESSION['userid']);
		render('home');
	}
	else
	{
		render('login');
		echo "<br><span style='color:red; font-weight:bold;'>Incorrect username or password</span>";
	}
}
else
{
	render('login');
}
?>
