<?php
/*********************
 * portfolio.php
 *
 * CSCI S-75
 * Project 1
 * Chris Gerber
 *
 * Portfolio controller
 *********************/

require_once('../model/model.php');
require_once('../includes/helper.php');

if (isset($_SESSION['userid']))
{
	// get the list of holdings for user
	$userid = $_SESSION['userid'];
	$holdings = get_user_shares($userid);
	
	// if user has no shares, alert
	if ($holdings == null)
		{
			render('home');
			echo ('<script type="text/javascript">alert ("Your portfolio is empty. Buy some shares through quote form.");</script>');
		}
		
	else
	{
	// get user's symbols in the correct format
	$sym_str = urlencode(get_user_symbols($userid));
	
	// query yahoo for the latest price
	$prices = get_quote_data($sym_str, urlencode('l1'));
	
	render('portfolio', array('holdings' => $holdings, 
								'prices' => $prices));
	}
}
else
{
	render('login');
}
?>
