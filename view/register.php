<?php
/* Registration view */


require_once('../includes/helper.php');
render('header', array('title' => 'C$75 Finance registration'));
?>

<form method="POST" name="register" id="register" action="register" onsubmit="return validateForm();">
    E-mail address: <input type="email" name="email" id="email" autofocus><br />
    Password: <input type="password" name="password" id="password"/><br />
    <span style="color:orange; font-size: 12px;" id="alert" name="alert">
Password should be at least 6 characters and should include at least one number and one letter.</span><br><br>
	<input type="submit" value="Sign Up" />
</form>

<script type='text/javascript'>


// set the focus to the email field (located by id attribute)
$("input[name=email]").focus();


function validateForm()
{
	//check if valid email is submitted
	var x=document.forms["register"]["email"].value;
	var atpos=x.indexOf("@");
	var dotpos=x.lastIndexOf(".");
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
	{
		alert("Not a valid e-mail address");
		return false;
	}
	var y=document.forms["register"]["password"].value;
	re = /[0-9]/;
	reg = /[a-z, A-Z]/;
	if (y.length < 6 || !re.test(y) || !reg.test(y))
	{
		document.getElementById("alert").style.border = "1px solid red";
		return false;
	}		
	
}


// set the focus to the email field (located by id attribute)
//$("input[name=email]").focus();

// ]] >
</script>
<? 
render('footer');
?>
