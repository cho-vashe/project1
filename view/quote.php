<?php
require_once('../includes/helper.php');
if (!isset($quote_data["symbol"]) || $quote_data["last_trade"] == 0)
{
    // No quote data
    render('header', array('title' => 'Quote'));
    print "No symbol was provided, or no quote data was found.";
}
else
{
    // Render quote for provided quote data
    render('header', array('title' => 'Quote for '.htmlspecialchars($quote_data["symbol"])));
?>
<script type='text/javascript'>

// set the focus to the email field (located by id attribute)
$("input[name=shares]").focus();

function validateQuantity()
{
	//check if valid email is submitted
	var x=document.forms["quote"]["shares"].value;
	if(typeof x==='number' && (x%1)===0 && x > 0) {
		return true;
	}
	else
		return false;
	
}
</script>
<table>
    <tr>
        <th>Symbol</th>
        <th>Name</th>
        <th>Last Trade</th>
    </tr>
    <tr>
        <td><?= htmlspecialchars($quote_data["symbol"]) ?></td>
        <td><?= htmlspecialchars($quote_data["name"]) ?></td>
        <td><?= htmlspecialchars($quote_data["last_trade"]) ?></td>
        <td><form method="POST" action="buy" id="quote"> 
			<input type="number" id="shares" name="shares" min="1" step="1" 
		placeholder="Enter quantity" style="width:120px; height:28px;" autofocus>
			<input type="hidden" name="symbol" value="<?= mysql_escape_string($quote_data['symbol']) ?>">
			<input type="submit" value="Buy shares" onsubmit="return validateQuantity;"></td>
    </tr>
</table>

<?php
}

render('footer');
?>
