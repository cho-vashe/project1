<?php
require_once('../includes/helper.php');
render('header', array('title' => 'Portfolio'));
$total = 0;
?>

<table>
    <tr>
        <th>Symbol</th>
        <th>Shares</th>
        <th>Latest Price</th>
        <th>Value</th>
        <th>Sell all</th>
    </tr>
<?php
$cash = get_user_balance($_SESSION['userid']);
for($i = 0; $i < count($prices); $i++)
{
    $value = $holdings[$i]["shares"] * $prices[$i];
    $total = $total + $value;
    print "<tr>";
    print "<td>" . htmlspecialchars($holdings[$i]["symbol"]) . "</td>";
    print "<td>" . htmlspecialchars($holdings[$i]["shares"]) . "</td>";
    print "<td>" . htmlspecialchars($prices[$i]). "</td>";
    print "<td>" . htmlspecialchars($value). "</td>";    
    print '<td><form method="POST" action="sell">
	<input name="symbol" type="hidden" value=' . $holdings[$i]["symbol"] . '>
	<input name="shares" type="hidden" value=' . $holdings[$i]["shares"] . '>
	<input type="submit" value="Sell"></form></td>';
    print "</tr>";    
}
	print "<tr class='total'>";
	print "<td colspan='5'>Stocks value: $" . $total . "</td></tr>";
    print "<tr class='total'>";
	print "<td colspan='5'>" . "Cash: $" . $cash . "</td></tr>";
    print "<tr class='total'>";
    print "<td colspan='5'>" . "Total portfolio value: $" . 
					$ttl = $cash + $total . "</td>";
	print "</tr>";
?>
</table>

<?php
render('footer');
?>
