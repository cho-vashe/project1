<?php
require_once('../includes/helper.php');
render('header', array('title' => 'C$75 Finance'));
?>

<ul>
	<li>Your balance is <?= get_user_balance($_SESSION['userid'])?></li>
	<li>
		<form method="POST" action="quote">
			Get quotes: <input type="text" name="param" placeholder="Enter stock symbol"/>
			<input type="submit" value="Look up" />
		</form>
	</li>
	<li><a href="portfolio">View Portfolio</a></li>
	<li><a href="logout">Logout</a></li>
</ul>

<?php
render('footer');
?>
