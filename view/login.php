<?php
require_once('../includes/helper.php');
render('header', array('title' => 'C$75 Finance'));
?>

<form method="POST" name="login" action="login" onsubmit="return validateForm();">
    E-mail address: <input type="email" name="email" autofocus><br />
    Password: <input type="password" name="password" /><br />
	<input type="submit" value="Login" />
</form>

<script type='text/javascript'>

// set the focus to the email field (located by id attribute)
$("input[name=email]").focus();

</script>
<a href="register">Sign up</a>
<?php
render('footer');
?>
