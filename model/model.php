<?php
/*********************************
 * model.php
 *
 * CSCI S-75
 * Project 1
 * Chris Gerber
 *
 * Model for users and portfolios
 *********************************/

// database credentials
define('DB_HOST', 'localhost');
define('DB_USER', 'jharvard');
define('DB_PASSWORD', 'crimson');
define('DB_DATABASE', 'project1');

/*
 * login_user() - Verify account credentials and create session
 *
 * @param string $email
 * @param string $password
 */
 
function login_user($email, $password)
{
    // connect to database through PDO
    if (($dbh = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD)) === false)
    {
		die("Could not connect to database");
	}
    
	$userid = 0;
	
    // query database
    $sql = $dbh->query("SELECT id FROM users WHERE email= '{$email}' AND password= '{$password}'");
    $userid = $sql->fetchColumn();
	return $userid;
    // close database
    $dbh = null;
    
}

/*
 * get_user_shares() - Get portfolio for specified userid
 *
 * @param int $userid
 */
function get_user_shares($userid)
{
	// connect to database with PDO
	if (($dbh = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD)) === false)
    {
		die("Could not connect to database");
	}
	
	// get user's portfolio
	$stmt = $dbh->prepare("SELECT symbol, shares FROM portfolios WHERE id=:userid");
	$stmt->bindValue(':userid', $userid, PDO::PARAM_INT);
	if ($stmt->execute())
	{
	    $result = array();
	    while ($row = $stmt->fetch()) {
			array_push($result, $row);
	    }
	  
		$dbh = null;
		return $result;
	}
	
	// close database and return null 
	$dbh = null;
	return null;
}

/*
 * get_quote_data() - Get Yahoo quote data for a symbol
 *
 * @param string $symbol
 */
function get_quote_data($symbol, $format)
{
	$result = array();
	$url = "http://download.finance.yahoo.com/d/quotes.csv?s={$symbol}&f={$format}&e=.csv";
	$handle = fopen($url, "r");
	if ($handle !== FALSE)
	{
		if($format == urlencode('sl1n'))
				{
					$row = fgetcsv($handle);
					if ($row !== FALSE && $row[1] !== 0.00)
					{
						if (isset($row[1]))
						$result = array("symbol" => $row[0],
								"last_trade" => $row[1],
								"name" => $row[2]);
					}
				}
		else if($format == urlencode('l1'))
		{
			while (!feof($handle)) 
			{
				$i = 0;
				$data = fgetcsv($handle, 1000, "\n");
				$result[] = $data[$i];
				$i++;
			}
		}
				
		fclose($handle);
		return $result;
	}
}

/*
 * register_user() - Create a new user account
 *
 * @param string $email
 * @param string $password
 * 
 * 
 */
function register_user($email, $password)
{
    $chkmail = filter_var($email, FILTER_VALIDATE_EMAIL);
    $chkpwd = preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,50}$/', $password);
    
    if($chkmail === false || !$chkpwd)
    {
		return false;
	}
	
	else
	{
    
    // connect to database through PDO
    $dbh = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD);
    
    // password hash
	$pwdhash = hash("SHA1",$password);
	
	//prepare database to receive a new row
	$add_user =  $dbh->prepare('INSERT INTO users (id, email, password) VALUES (NULL, :email, :password)');
	
	//bind values to parameters
	$add_user->bindValue(':email',$email,PDO::PARAM_STR);
	$add_user->bindValue(':password',$pwdhash,PDO::PARAM_STR);
	
	// query the result
	$add_user->execute();
	
	//add userid
	$userid = $dbh->lastInsertId();
	return $userid;
	
    // close database
    $dbh = null;
	}
    
    

}

function get_user_balance($userid)
{
    // connect to database through PDO
    if (($dbh = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD)) === false)
    {
		die("Could not connect to database");
	}
 
    // query database
    $sql = $dbh->query("SELECT balance FROM users WHERE id= '{$userid}'");
    $userblc = $sql->fetchColumn();
	
	return $userblc;
    
    // close database
    $dbh = null;
    
}

function buy_shares($userid, $symbol, $shares) 
{
	$userid = $_SESSION['userid'];
	
	//check input (not a float, positive int)
	if (filter_var($shares, FILTER_VALIDATE_INT) !== false && $shares > 0)
	{
		// connect to database through PDO
		$dbh = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD);
		
		// begin the transaction
		$dbh->beginTransaction();
		
		// get quote data to get the latest price
		$qry = get_quote_data($symbol, urlencode('l1'));
		
		//count transaction sum
		$sum = $qry[0] * $shares;
		
		//update balance
		$upd = $dbh->prepare("UPDATE users 
							SET balance = balance - :sum 
							WHERE  id = :id");
		$upd->bindValue(':sum', $sum);
		$upd->bindValue(':id', $userid, PDO::PARAM_STR);
		$upd->execute();
		
		
		//add shares to portfolio
		$addshr = $dbh->prepare(
			"INSERT INTO portfolios (id, symbol, shares) 
			VALUES (?, ?, ?) 
			ON DUPLICATE KEY UPDATE shares=shares+?");
		$addshr->execute(array($userid, $symbol, $shares, $shares));
		
		//check if there is enough money
		$chkblc = ("SELECT * FROM users WHERE balance <0 AND id = ?");
		$r = $dbh->prepare($chkblc);
		$r->execute(array($userid));
		$stat = $r->fetch(PDO::FETCH_BOTH);
		if ($stat[0] == NULL)
		{
			$dbh->commit();
			$dbh = null;
			return 1;
		}
		else
		{
			$dbh->rollback();
			$_SESSION['r'] = $stat;
			$dbh = null;
			return 0;
		}
	}
	else
	return -1;
}
		
			

function sell_shares($userid, $symbol, $shares) {
	
	// connect to database through PDO
    if (($dbh = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD)) === false)
    {
		die("Could not connect to database");
	}
	
	// begin the transaction
	$dbh->beginTransaction();
	
	// query the Yahoo for the latest price
	$qry = get_quote_data($symbol, urlencode('l1'));
	
	// count sale income
	$sum = $qry['0'] * $shares;
	
	// update balance
	$upd = $dbh->prepare("UPDATE users 
						SET balance = balance + :sum 
						WHERE  id = :id");
	$upd->bindValue(':sum', $sum);
	$upd->bindValue(':id', $userid, PDO::PARAM_STR);
	$upd->execute();
	
	// delete shares entry from portfolio
	$del = $dbh->prepare("DELETE FROM portfolios WHERE id=:id AND symbol=:symbol");
	$del->bindValue(':id', $userid, PDO::PARAM_INT);
    $del->bindValue(':symbol', $symbol, PDO::PARAM_STR);
	$del->execute();
		
	// commit
	$dbh->commit();
	
	//close connection
	$dbh = null;
	return null;
	
	}

/*
 * get_user_symbols - Fetches an array from portfolios and formats
 * it as SYM+SYM as yahoo finance requires
 * @param int $userid
 * 
 */

function get_user_symbols($userid)
{
	// connect to database with PDO
	if (($dbh = new PDO('mysql:host=' .DB_HOST . ';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD)) === false)
    {
		die("Could not connect to database");
	}
	
	// get user's portfolio's symbols
	$stmt = $dbh->prepare("SELECT symbol FROM portfolios WHERE id=:userid");
	$stmt->bindValue(':userid', $userid, PDO::PARAM_INT);
	if ($stmt->execute())
	{
	    $result = array();
	    while ($row = $stmt->fetch()) {
			array_push($result, $row['symbol']);
	    }
		$dbh = null;
	}
	// close database and return null 
	$dbh = null;
	
	//format array as string
	$symb_str=implode(' ', $result);
	return $symb_str;	
}
